const divideClassForm = {
    "settings": {
        "i18n": {
            "en": {
                "Title": "Title"
            },
            "el": {
                "Delete (-)": "Διαγραφή (-)",
                "Cancel": "Ακύρωση",
                "Title": "Τίτλος",
                "Instructors": "Διδάσκοντες",
                "Select the instructors who will associated with this class part": "Επιλέξτε τους διδάσκοντες που θα συνδεθούν με αυτό το μέρος της τάξης.",
                "This title will be used as a suffic of the the course class title.": "Αυτός ο τίτλος θα χρησιμοποιηθεί ως συμπλήρωμα στον τίτλο της τάξης.",
                "No choices to choose from": "Δεν υπάρχουν άλλες επιλογές"
            }
        }
    },
    "components": [
        {
            "label": "Title",
            "description": "This title will be used as a suffic of the the course class title.",
            "tableView": true,
            "validate": {
                "required": true
            },
            "key": "title",
            "type": "textfield",
            "input": true
        },
        {
            "label": "Instructors",
            "widget": "choicesjs",
            "description": "Select the instructors who will associated with this class part",
            "customConditional": "show = data.courseClass != null",
            "tableView": true,
            "validate": {
                "required": true
            },
            "dataSrc": "url",
            "data": {
                "url": "/CourseClassInstructors?$filter=courseClass eq '{{data.courseClass.id}}'&$select=instructor/id as id, instructor/familyName as familyName, instructor/givenName as givenName"
            },
            "template": "{{ item.familyName }} {{ item.givenName }}",
            "dataType": "object",
            "multiple": true,
            "idProperty": "item.id",
            "selectValues": "value",
            "key": "instructors",
            "type": "select",
            "input": true,
            "disableLimit": false,
            "noRefreshOnScroll": false
        }
    ]
}

export {
    divideClassForm
}
