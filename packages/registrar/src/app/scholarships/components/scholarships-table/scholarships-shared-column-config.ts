import { TableColumnConfiguration } from '@universis/ngx-tables';

export const SCHOLARSHIP_SHARED_COLUMNS: TableColumnConfiguration[] = [
  {
    name: 'id',
    property: 'scholarshipId',
    title: 'Scholarships.Shared.Id',
    hidden: true,
    optional: true
  },
  {
    name: 'name',
    property: 'scholarshipName',
    title: 'Scholarships.Shared.Name',
    hidden: true,
    optional: true
  },
  {
    name: 'totalStudents',
    property: 'scholarshipTotalStudents',
    title: 'Scholarships.Shared.TotalStudents',
    hidden: true,
    optional: true
  },
  {
    name: 'department/abbreviation',
    property: 'scholarshipDepartmentAbbreviation',
    title: 'Scholarships.Shared.DepartmentAbbreviation',
    hidden: true,
    optional: true
  },
  {
    name: 'organization/name',
    property: 'scholarshipOrganizationName',
    title: 'Scholarships.Shared.OrganizationName',
    hidden: true,
    optional: true
  },
  {
    name: 'status/alternateName',
    property: 'scholarshipStatusAlternateName',
    title: 'Scholarships.Shared.StatusAlternateName',
    formatter: 'TranslationFormatter',
    formatString: 'Scholarships.StatusAlternateName.${value}',
    hidden: true,
    optional: true
  },
  {
    name: 'scholarshipType/alternateName',
    property: 'scholarshipScholarshipTypeAlternateName',
    title: 'Scholarships.Shared.ScholarshipTypeAlternateName',
    formatter: 'TranslationFormatter',
    formatString: 'Scholarships.ScholarshipType.${value || "none"}',
    hidden: true,
    optional: true
  },
  {
    name: 'year/alternateName',
    property: 'scholarshipYearAlternateName',
    title: 'Scholarships.Shared.YearAlternateName',
    hidden: true,
    optional: true
  }
]
