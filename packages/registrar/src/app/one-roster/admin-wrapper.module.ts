import { ActivatedUser, ConfigurationService, DiagnosticsService } from '@universis/common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { SettingsService } from '../settings-shared/services/settings.service';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { SharedModule } from '@universis/common';
import { NgxOneRosterAdminModule } from '@universis/ngx-one-roster/admin';

@NgModule({
  imports: [
      TranslateModule,
      SharedModule,
      NgxOneRosterAdminModule
  ]
})
export class OneRosterAdminWrapperModule {

    constructor(private activatedUser: ActivatedUser, private configuration: ConfigurationService, private translate: TranslateService, private settings: SettingsService, private diagnostics: DiagnosticsService) {
        this.activatedUser.user.subscribe((user) => {
          if (user) {
            setTimeout(() => {
              this.diagnostics.hasService('OneRosterService').then((hasService) => {
                if (hasService) {
                  this.settings.addSection({
                      name: 'OneRosterSyncAction',
                      description: this.translate.instant('Settings.Lists.OneRosterSyncAction.Description'),
                      longDescription: this.translate.instant('Settings.Lists.OneRosterSyncAction.LongDescription'),
                      category: 'OneRoster',
                      url: '/one-roster/actions'
                    });
                }
              }).catch((err: Error) => console.error(err));
            });
          }
        });
      }

    static forRoot() : ModuleWithProviders<OneRosterAdminWrapperModule> {
      return {
        ngModule: OneRosterAdminWrapperModule,
        providers: []
      }
    }

}