import { StatisticsModule } from './statistics.module';
import { TestBed } from '@angular/core/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { StatisticsRoutingModule } from './statistics.routing';
import { NgArrayPipesModule } from 'ngx-pipes';
import { ReportsSharedModule } from '../reports-shared/reports-shared.module';
import { StatisticsService } from './services/statistics-service/statistics.service';
import { SectionsComponent } from './components/sections/sections.component';
import { HomeComponent } from './components/home/home.component';
import { SettingsModule } from './../settings/settings.module';
import { MostModule } from '@themost/angular';
import { SettingsService } from './../settings-shared/services/settings.service';
import { AuthModule, ConfigurationService } from '@universis/common';
import {TestingConfigurationService } from '@universis/common/testing';

describe('StatisticsModule', () => {
  let statisticsModule: StatisticsModule;

  beforeEach(() => {
    return TestBed.configureTestingModule({
      imports: [
        CommonModule,
        AuthModule,
        TranslateModule.forRoot(),
        StatisticsRoutingModule,
        FormsModule,
        SettingsModule,
        MostModule.forRoot({
          base: '/',
          options: {
            useMediaTypeExtensions: false
          }
        }),
        NgArrayPipesModule,
        ReportsSharedModule.forRoot()
      ],
      declarations: [HomeComponent, SectionsComponent],
      providers: [
        SettingsService,
        StatisticsService,
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        }
      ]
    }).compileComponents();
  });
  
  it('should create an instance', () => {
    const translateService = TestBed.get(TranslateService);
    statisticsModule = new StatisticsModule(translateService);
    expect(statisticsModule).toBeTruthy();
  });
});
