import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentDownloadRoutedComponent } from './document-download-routed.component';

describe('DocumentDownloadRoutedComponent', () => {
  let component: DocumentDownloadRoutedComponent;
  let fixture: ComponentFixture<DocumentDownloadRoutedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentDownloadRoutedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentDownloadRoutedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
