import { SettingsModule } from './settings.module';
import { ConfigurationService, SharedModule } from '@universis/common';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { TestBed, async, inject } from '@angular/core/testing';
import { MostModule } from '@themost/angular';
import { ApiTestingModule, TestingConfigurationService } from '@universis/common/testing';
import { APP_BASE_HREF } from '@angular/common';

describe('SettingsModule', () => {
  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot(),
        MostModule.forRoot({
          base: '/',
          options: {
            useMediaTypeExtensions: false
          }
        }),
        ApiTestingModule.forRoot()
      ],
      providers: [
        {
          provide: APP_BASE_HREF,
          useValue: '/'
        },
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        }
      ],
    }).compileComponents();
  }));

});
