{
  "Exams": {
    "Title": "Exams",
    "TitleSingular": "Exam",
    "All": "All",
    "Any": "Any",
    "New": "New",
    "Export": "Export",
    "Filter": "Search",
    "CurrentYear": "Current academic year",
    "SearchPlaceholder": "Display code, course title, etc.",
    "Id": "Id",
    "Actions": "Actions",
    "GradeScale": "Exam gradescale",
    "DisplayCode": "Course display code",
    "CourseName": "Title",
    "CourseNamePlaceholder": "Type part of the title",
    "Year": "Year",
    "Period": "Period",
    "NumberOfGradedStudents": "Graded students",
    "Status": "Exam Status",
    "DateCompleted": "Date completed",
    "CompletedByUser": "Completed by",
    "Completed": "Completed",
    "ExamName": "Exam title",
    "ExamDate": "Exam date",
    "ResultDate": "Result date",
    "AddResultDateMessage": {
      "Title": "Set result date in exams",
      "Description": "With this action you can set the result date for the selected course exams in bulk. Attention: if the date is already set, it will be replaced with the new date.",
      "DescriptionOne": "",
      "NoItems": "The selected exams should refer to the same exam period and year."
    },
    "DecimalDigits": "Decimal digits",
    "Notes": "Notes",
    "Preview": "Preview",
    "General": "Overview",
    "Students": "Students",
    "ExamInfo": "Exam Info",
    "Grade": "Grade",
    "Instructors": "Instructors",
    "Grading": {
      "Singular": "Grades Submission",
      "Plural": "Grades Submissions"
    },
    "Instructor": "Instructor",
    "EditDate": "Edit Date",
    "Time": "Time",
    "ExamPeriod": "Exam Period",
    "Check": "Check",
    "SubmittedGrades": "Submitted Grades",
    "Show": "Show",
    "DocumentCode": "Document Code",
    "SuccessfulCheck": "Successful check",
    "FailedCheck": "Unsuccessful check",
    "NoUploadActions": "There are no grade submissions / edits",
    "Submitted": "Submitted",
    "UploadNumber": "Upload Number",
    "ECTS": "ECTS",
    "Units": "Units",
    "LateExam": "Late exam",
    "DigitalSignature": "Digital Signature",
    "DigitalSignatureTrue": "Contains Digital Signature",
    "DigitalSignatureFalse": "Does not contain Digital Signature",
    "CheckSuccessful": "Check Successful",
    "CheckUnsuccessful": "Check Unsuccessful",
    "TotalGrades": "Grades Total",
    "Reject": "Reject",
    "Approve": "Approve",
    "BackToList": "Back to list",
    "Edit": "Edit",
    "Choose": "Choose",
    "Totals": "Sums",
    "ChooseDate": "Choose date",
    "Statistics": "Statistics",
    "AddInstructor": "Add instructor",
    "Username": "Username",
    "NoInstructors": "No available instructors",
    "NoStatistics" : "No available statistics",
    "Attachment": "Related file",
    "ExamDocuments": "Grade submissions",
    "ExamDocument": "Grade submission",
    "ExamsParticipations": "Exam Participations",
    "AgreeTitle": "Agreement",
    "Verified": "Verified",
    "NotVerified": "Not verified",
    "VerificationFailed": "Verification failed",
    "DigitalSignNotValid": "The digital signature is not valid",
    "DigitalSignValid": "The degital signature is valid",
    "AddInstructorsMessage": {
      "title": "Add instructor in exam",
      "one": "A new instructor has been added successfully.",
      "many": "{{value}} new instructors have been added successfully."
    },
    "RemoveInstructorsMessage": {
      "title": "Remove instructor from exam",
      "one": "An instructor has been successfully removed.",
      "many": "{{value}} instructors have been successfully removed."
    },
    "RemoveInstructor": "Remove",
    "RemoveInstructorTitle": "Remove instructor",
    "RemoveInstructorMessage": "You are going to remove one or more instructors of this exam. Do you want to proceed?",
    "Info": "Exam information",
    "Chart": {
      "Statistics":"Statistics",
      "Graded": "Graded",
      "Successful":"Successful",
      "NumberOfStudents": "NUMBER OF STUDENTS",
      "GroupByGrade": "Group by grade",
      "Grade" : "GRADE",
      "Close" : "Close"
    },
    "CourseInfo": "Course information",
    "NoGeneralInfo": "No information",
    "MinNumberOfGradedStudents": "Min number of graded students",
    "MaxNumberOfGradedStudents": "Max number of graded students",
    "AddTestTypesMessage": {
      "title": "Add test type in exam",
      "one": "A new test type has been added successfully.",
      "many": "{{value}} new test types have been added successfully."
    },
    "RemoveTestTypesMessage": {
      "title": "Remove test type from exam",
      "one": "A test type has been successfully removed.",
      "many": "{{value}} test types have been successfully removed."
    },
    "AddTestType": "Add",
    "RemoveTestType": "Remove",
    "RemoveTestTypeTitle": "Remove test type",
    "RemoveTestTypeMessage": "You are going to remove one or more test types of this exam. Do you want to proceed?",
    "TestTypes": "Test types",
    "TestType": "Test type",
    "Description": "Description",
    "OpenExam": "Open",
    "CloseExam": "Close",
    "DeleteExam": "Delete",
    "OpenAction": {
      "Title": "Open course exams",
      "Description": "This action is going to open the selected course exams. Instructors will be able to change course exams by uploading student grades during grade period.",
      "DescriptionOne": "This action is going to open the selected course exam. Instructors will be able to change course exams by uploading student grades during grade period."
    },
    "CloseAction": {
      "Title": "Close course exams",
      "Description": "This action is going to close the selected course exams. Students are not able to change course exams that have been closed. The action can be performed for open or completed exams that are not linked to any active/pending grade submissions.",
      "DescriptionOne": "This action is going to close the selected course exam. Students are not able to change course exams that have been closed. The action can be performed for open or completed exams that are not linked to any active/pending grade submissions."
    },
    "OpenActionFilter": {
      "title": "Invalid filter",
      "message": "Smart selection of all items requires a filter for course exam status. Use filter and set a value for exam status other than \"Open\". Press \"Search\" button to refresh items."
    },
    "CloseActionFilter": {
      "title": "Invalid filter",
      "message": "Smart selection of all items requires a filter for course exam status. Use filter and set a value for exam status other than \"Closed\". Press \"Search\" button to refresh items."
    },
    "DeleteAction": {
      "Title": "Delete course exams",
      "Description": "This action is going to delete selected course exams. Deletion is not allowed if students have been graded.",
      "CompletedWithErrors": {
        "Title": "The operation was completed with errors.",
        "Description": {
          "One": "One course exam has not been deleted due to an error occurred or due to graded students.",
          "Many": "{{errors}} course exams have not been deleted due to an error occurred or due to graded students"
        }
      }
    },
    "AcceptGrades": {
      "title": "Accept grades",
      "message": "You are about to accept exam grades. Do you want to proceed;",
      "success": "Grades accepted successfully "
    },
    "RejectGrades": {
      "title": "Reject grades",
      "message": "You are about to reject exam grades. Do you want to proceed;",
      "success": "Grades rejected successfully "
    },
    "GradeStatus" : {
      "pending": "Pending",
      "normal": "Regular",
      "remark": "Remark",
      "failed": "Failed",
      "null": "-"
    },
    "SignatureInfo": {
      "CheckHash": "Check hash",
      "SignatureBlock": "Signature block",
      "Issuer": "Issued by",
      "Subject": "Subject",
      "SerialNumber": "Serial number",
      "Version": "Version",
      "NotValidBefore": "Starting validity date",
      "NotValidAfter": "Expiration date",
      "Algorithm": "Algorithm",
      "PublicKey": "Public Key",
      "PublicKeyParameters": "Public key parameters",
      "Field": "Field",
      "Value": "Value",
      "General": "General",
      "Details": "Details",
      "CertificateInformation": "Certificate Information",
      "IssuedTo": "Issued to",
      "NotValidBeforeShort": "Valid from",
      "NotValidAfterShort": "to",
      "CertificatePurposes": {
        "Header": "This certificate is used for the following purposes:",
        "emailProtection": "Protect e-mail messages",
        "documentSign": "Document Signing",
        "clientAuth": "Prove your identity to a remote computer",
        "smartCardLogon": "Allow logging on to devices with a smart card",
        "nonRepudiation": "Non repudiation",
        "digitalSignature": "Digital Signature",
        "documentEnc": "Document Encryption",
        "serverAuth": "Prove your server identity",
        "codeSigning": "Code signing",
        "timestamping": "Timestamping",
        "keyEncipherment": "Key encipherment",
        "any": "Any other purpose"
      }
    },
    "NewGradeSubmission": "New Submission",
    "UploadGradingScore": "Upload the xlsx file containing the grades of the students. You can submit a sheet with empty grade columns and also delete one or more grades, by using a dash \"-\" in the corresponding grade column.",
    "GradingScoreHelp": "After you upload and initially submit the grades, their integrity will be checked and you will be informed. If all grades are valid, you can then proceed to the final submission.",
    "InitialSubmission": "Initial Submission",
    "InitialSubmissionMessage": "You are about to perform the initial submission of the grades. Do you wish to proceed?",
    "FinalSubmission": "Final Submission",
    "FinalSubmissionMessage": "You are about to perform the final submission of the grades. Do you wish to proceed?",
    "DownloadGradingSheet": "Download Grade Table",
    "DownloadGradingSheetMessage": "You are about to download the exam's grade table. Do you wish to proceed?",
    "SignActionTitle": "Grade table digital signature",
    "SignActionDescription": "The operation will try to digitally sign the uploaded grade table and then validate its grades.",
    "GradeStatusTitle": "Grade status",
    "GradeStatusTypes": {
      "UPD": "Will update existing grade",
      "INS": "New grade",
      "UNMOD": "Will not be modified",
      "ESTATUS": "The student is not active",
      "EAVAIL":"The student is not available in this course exam",
      "EFAIL":"Failed",
      "ERANGE":"The grade is out of the exam's grade scale range",
      "INVDATA":"Invalid",
      "EPASS": "There is already a passed grade in another exam or there is an exemption",
      "E_DENY_GRADING": "The student cannot be graded (e.g. due to incomplete attendance)"
    },
    "SuccessError": "An error occured while validating the grades",
    "SuccessErrorMessage": "One or more grades are invalid. You can see which one(s) in the list below.",
    "SheetSubmissionFeedback": {
      "title": "Grade table submission",
      "message": "The grade table has been successfully submitted with status \"Pending\". You will be redirected to its approval's interface."
    },
    "InvalidGradingSheetError": "The grade table you submitted is invalid.",
    "SubmittedModifiedGrades": "Total modified grades",
    "InvalidSignature": "The digital signature is invalid",
    "NoModifiedGrades": "The grade table contains grades, but all of them are already added to the system.",
    "SubmissionStatus": "Submission status",
    "VerifyDigitalSignature": "Verify digital signature",
    "DigitalSignatureControl": {
      "Valid": "Contains a valid digital signature",
      "Invalid": "Contains an invalid digital signature",
      "NoSignature": "Does not contain a digital signature",
      "NotDetermined": "Cannot be verified. Either the signer service is offline, or an error occured during the verification process.",
      "NotDeterminedInfo": "You can proceed with approving the grades. The digital signature verification will be performed on the server side."
    },
    "AcceptAction": {
      "SuccessfulCompletion": "Successful grade submission approval",
      "FailedCompletion": "Failed grade submission approval, an error occured during the process",
      "ActionInProgress": "The grade submission approval is in-progress",
      "CancelledByTheUser": "The approval process has been cancelled by the user",
      "ToastInfo": "The approval process has begun and may require several minutes to be completed. You will receive a notification once the process is completed.",
      "ErrorTitle": "Approval error message",
      "CancelAction": {
        "ModalTitle": "Cancel submission approval process",
        "ModalDescription": "You are about to cancel the grades submission approval process. All the grade changes-up until now-will be undone. Do you wish to proceed?",
        "Tooltip": "You can, if you wish,",
        "Anchor": "cancel",
        "Next": "the submission approval process"
      },
      "CompletionToast": {
        "Title": "Grade submission approval {{name}}",
        "Message": "A pending grade submission approval has been completed. Follow the link below to see the results.",
        "LinkTitle": "Results"
      }
    },
    "RejectAction": {
      "SuccessfulCompletion": "Successful grade submission rejection",
      "FailedCompletion": "Failed grade submission rejection, an error occured during the process",
      "ActionInProgress": "The grade submission rejection is in-progress",
      "CancelledByTheUser": "The rejection process has been cancelled by the user",
      "ToastInfo": "The rejection process has begun and may require several minutes to be completed. You will receive a notification once the process is completed.",
      "ErrorTitle": "Rejection error message",
      "CancelAction": {
        "ModalTitle": "Cancel submission rejection process",
        "ModalDescription": "You are about to cancel the grades submission rejection process. All the grade changes-up until now-will be undone. Do you wish to proceed?",
        "Tooltip": "You can, if you wish,",
        "Anchor": "cancel",
        "Next": "the submission rejection process"
      },
      "CompletionToast": {
        "Title": "Grade submission rejection {{name}}",
        "Message": "A pending grade submission has been rejected. Follow the link below to see the results.",
        "LinkTitle": "Results"
      }
    },
    "ActionAlreadyCompleted": {
      "Title": "The action has been completed",
      "Description": "The action has been completed. Please refresh the page to see its results."
    },
    "Cancel": "Cancel",
    "FailedGrades": "Failed",
    "FailedGradesInfo": "While approving the grade's submission, some grades failed to be registrered to the system. You can see them in the list below (you can, if you like, use the relevant search filter). For these grades to be registered correctly, there needs to be a new submission/approval process.",
    "ExportGradeTable": "Export grade table",
    "ExportStudentsList": "Export list",
    "CalculateStatisticsAction": {
      "Title": "Calculate exam statistics",
      "Description": "With this action you can calculate statistics for the selected exams such as number of students who can participate to exams, how many were examined and how many got a passing grade.",
      "CompletedWithErrors": {
        "Title": "The operation was completed with errors.",
        "Description": {
          "One": "Course exam statistics were not calculated due to an error",
          "Many": "Course exam statistics were not calculated due to an error for {{errors}} exams."
        }
      }
    },
    "PendingGrades": "Pending grades",
    "NumberOfParticipants": "Can participate",
    "Passed": "Passed",
    "WithPendingGrades": "With pending grades",
    "StatisticsHelp": "To recalculate data such as the number of students that can take the exam, how many were examined and how many received a passing grade, select the exams and perform the \"Calculate Exam Statistics\" action",
    "SharedColumns":{
      "id": "Exam ID",
      "description": "Exam description",
      "notes": "Exam notes",
      "isLate": "Late exam (YES/NO)",
      "examDate": "Exam date",
      "status": "Exam status",
      "isCalculated": "Calculated exam",
      "decimalDigits": "Exam grade decimal digits",
      "name": "Exam name",
      "resultDate": "Exam results date",
      "numberOfGradedStudents": "Number of graded students of exam",
      "dateCompleted": "Exam completion date",
      "dateModified": "Exam modification date",
      "gradeScale": "Exam gradescale",
      "types": "Exam type"
     },
     "SemesterFrom": "Semester from",
     "SemesterTo": "Semester to",
     "Graded": "Graded",
     "Successful": "Successful"
  },
  "AdvancedSearch": {
    "PreDefinedLists": {
      "pendingExams": "Pending open exams",
      "currentExams": "Exams of current academic year"
    }
  }
}
