import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
// tslint:disable-next-line:max-line-length
import {ActivatedRoute, Router} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import { DatePipe } from '@angular/common';
import {
    AdvancedTableModalBaseComponent,
    AdvancedTableModalBaseTemplate,
    TableConfiguration
} from '@universis/ngx-tables';
import {AdvancedFilterValueProvider} from '@universis/ngx-tables';
import { TableConfigurationResolver } from '../../../registrar-shared/table-configuration.resolvers';

@Component({
    selector: 'app-select-course',
    template: AdvancedTableModalBaseTemplate
})
export class SelectCourseComponent extends AdvancedTableModalBaseComponent implements OnInit {

    @Input() tableConfig: TableConfiguration;
    constructor(_router: Router, _activatedRoute: ActivatedRoute,
                _context: AngularDataContext,
                protected advancedFilterValueProvider: AdvancedFilterValueProvider,
                protected datePipe: DatePipe,
                private resolver: TableConfigurationResolver) {
        super(_router, _activatedRoute, _context, advancedFilterValueProvider, datePipe);
    }
    
    ngOnInit() {
        super.ngOnInit();
    }

    hasInputs(): Array<string> {
        return [];
    }

    ok(): Promise<any> {
        return this.close();
    }
}
