import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-theses-dashboard-overview-general',
  templateUrl: './theses-dashboard-overview-general.component.html',
})
export class ThesesDashboardOverviewGeneralComponent implements OnInit, OnDestroy {

  @Input() thesis: any;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {}

  async ngOnInit() {
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
