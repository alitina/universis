import { Component, Input, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { DatePipe } from '@angular/common';
import { AdvancedTableSearchBaseComponent } from '@universis/ngx-tables';

@Component({
    selector: 'app-study-programs-advanced-table-search',
    templateUrl: './study-programs-advanced-table-search.component.html',
    encapsulation: ViewEncapsulation.None
})

export class StudyProgramsAdvancedTableSearchComponent extends AdvancedTableSearchBaseComponent {

    constructor(_context: AngularDataContext, _activatedRoute: ActivatedRoute, datePipe: DatePipe) {
        super();
    }
}
