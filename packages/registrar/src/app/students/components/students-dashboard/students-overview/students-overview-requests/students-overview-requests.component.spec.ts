import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsOverviewRequestsComponent } from './students-overview-requests.component';

describe('StudentsOverviewRequestsComponent', () => {
  let component: StudentsOverviewRequestsComponent;
  let fixture: ComponentFixture<StudentsOverviewRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsOverviewRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsOverviewRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
