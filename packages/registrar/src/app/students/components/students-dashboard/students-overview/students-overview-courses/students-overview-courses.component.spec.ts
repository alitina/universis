import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsOverviewCoursesComponent } from './students-overview-courses.component';

describe('StudentsOverviewCoursesComponent', () => {
  let component: StudentsOverviewCoursesComponent;
  let fixture: ComponentFixture<StudentsOverviewCoursesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsOverviewCoursesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsOverviewCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
